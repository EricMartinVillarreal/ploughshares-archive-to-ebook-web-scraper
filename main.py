import requests
from bs4 import BeautifulSoup
from ebooklib import epub

issue_number = input('What issue number?')

headers = {
    'user-agent': 'Mozilla/5.0 \
    (Macintosh; Intel Mac OS X 10_11_6) \
    AppleWebKit/537.36 (KHTML, like Gecko) \
    Chrome/53.0.2785.143 Safari/537.36'
}

timeout = 200  # adjust this as needed either higher or lower

user_login_url = 'https://www.pshares.org/user'


def get_form_build_id():
    r = requests.get(user_login_url, headers=headers, timeout=timeout)
    soup = BeautifulSoup(r.text, 'html.parser')
    hidden_string = str(soup.find(type="hidden"))
    value_string = hidden_string.split('value="')[1]
    value_string = value_string.split('"')[0]

    return value_string


form_data = {
    'name': 'your_username',  # change this
    'pass': 'your_password',  # change this
    'form_build_id': get_form_build_id(),  # DO NOT CHANGE
    'form_id': 'user_login'  # DO NOT CHANGE
}


def master():
    session = requests.Session()
    login_to_pshares(session)
    create_ebook(session, get_issue_url(session, issue_number))

    return


def login_to_pshares(s):
    s.post(user_login_url, data=form_data, headers=headers)
    return


def get_issue_url(s, issue):
    url_list = []
    inc = 0
    while len(url_list) < 1:
        r = s.get('https://www.pshares.org/issues?page='+str(inc), headers=headers, timeout=timeout)
        soup = BeautifulSoup(r.text, 'html.parser')
        page_list = soup.find_all('li')

        a_list = []
        for entry in page_list:
            if str('Issue #    ' + str(issue)) in str(entry):
                soup = BeautifulSoup(str(entry), 'html.parser')
                a_list.append(soup.find('a'))

        for a in a_list:
            url_list.append(a.get('href'))
        inc += 1
    global short_url
    short_url = url_list[0]
    long_url = str('https://www.pshares.org') + short_url
    return long_url


def create_ebook(s, url):

    book = epub.EpubBook()

    book.set_language('en')

    c = []
    book.spine = ['nav']

    r = s.get(url, headers=headers, timeout=timeout)
    soup = BeautifulSoup(r.text, 'html.parser')

    title_temp = str(soup.title.string)
    title_temp = title_temp.split(' |')[0]
    book.set_title('Ploughshares ' + title_temp)

    split_point = 'Issue ' + str(issue_number) + ' |\n' + title_temp

    a_list = soup.find_all('a')

    a_href = []
    for link in a_list:
        if str(short_url) in str(link):
            a_href.append(link.get('href'))

    a_href.pop(0)

    inc = 0
    for link in a_href:
        r = s.get('https://www.pshares.org' + link, headers=headers, timeout=timeout)
        soup = BeautifulSoup(r.text, 'html.parser')

        story_string = str(soup.get_text()).split(split_point, 1)[1]
        story_string = story_string.split('\n\n\n', 1)[0]
        # Creates string of title, author, and story

        title_string = str(soup.title.string).split(' |', 1)[0]

        # Creates a string of just the title

        story_string = story_string.split(title_string, 1)[1]
        author_string = story_string.split('\n')[2]
        # Creates a String of just 'by author_name'

        story_string = story_string.split(author_string, 1)[1]
        story_string = story_string.replace('\n', '<br>')
        # Creates a string of just the story

        file_name = 'chap_' + str(inc + 1) + '.xhtml'

        c.append(epub.EpubHtml(title=title_string, file_name=file_name, lang='en'))
        c[inc].content = u'<h1>' + title_string + '</h1>' + '\n\n' + '<h3>' + author_string + '</h3>' + \
                         '\n\n''<p>' + story_string + '</p>' + '\n\n\n\n'

        # book.add_item(c[inc])
        inc += 1
        print(str(inc) + ' of ' + str(len(a_href)))

    style = 'body { font-family: Times, Times New Roman, serif; }'
    nav_css = epub.EpubItem(uid="style_nav", file_name="style/nav.css", media_type="text/css", content=style)

    book.add_item(nav_css)
    for item in range(len(c)):
        book.add_item(c[item])

    book.toc = c
    book.spine = ['nav']

    for item in range(len(c)):
        book.spine.append(c[item])

    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())

    epub.write_epub('Ploughshares_' + str(issue_number) + '_(' + title_temp + ')' + '.epub', book)

    return


master()
