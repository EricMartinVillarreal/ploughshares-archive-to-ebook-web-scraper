Clone this directory to your drive, then edit the user name and password.
Adjust the timeout variable if you are having timeout issues.

Run the python script, input the issue number you seek when prompted. Only input a numerical integer.

As it downloads the stories it will notify you of its progress in the terminal.

Once it finishes, the ebook will be available in the same folder you ran main.py

It is still buggy and may not work for every issue. If you find an issue that it doesn't work for please bug report it so I can fix the code.